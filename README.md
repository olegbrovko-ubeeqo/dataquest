# Dataquest for UbeeDataSharing

This is a small dataset on day-to-day operations of UbeeDataSharing.

## About us

UbeeDataSharing is a medium-sized but quickly-growing carsharing company basing its business on the round-trip model. This means that vehicles are located in fixed parkings or in a reasonably small geofence on the streets of a city and have to be returned to those locations at the end of the booking. The vehicles can be easily booked online or via a mobile app and picked up and returned at any time of day and night (one simply opens and locks the vehicle with one's smartphone or a registered NFC card).

Upon booking a UbeeDataSharing vehicle the user has to specify the reservaion start and end times. The real reservation end and number of km driven are recorded when the user has returned the vehicle to its designated location (parking) and locked it.

To better fit the needs of the customers UbeeDataSharing provides vehicles of different categories (ranging from mini to a MPVs and vans).

Since different users have different mobility needs we aim to offer our users flexible pricing. Normal *carsharing* bookings shall be charged bu the hour with 15 min granularity (minimal booking duration is 1 hour). For those customers who would like to use a UbeeDataSharing vehicle to have a day out of the city we offer *long-distance* reservation option with more economical time and mileage pricing.

Once in a while the vehicles require maintainance or cleaning. In those cases our *internal* agents shall make a *service* booking on the vehicle.

If you are curious to learn more about the business model of UbeeDataSharing, you shall find more information [right here](ubeeqo.com).

---

## The "big question"

As a young and rapidly maturing company UbeeDataSharing has finally arrived at the point of understanding that data is the key to the bright and shiny future and is now trying to become data driven in as many business domains as possible (operations, marketing, business). **Want to join us on our epic (most things are relative in this world) journey?** Then help us explore the full potential of our data, a small sample of which you shall find below!

---

## Datasets available

UbeeDataSharing uses a relational database to store it's data adhering to a normalised domain-segmented data model. In the `/data` directory of the present repository you shall find the following data:

### Operators

The company is subdivided into several operators providing service in different countries.

File: `/data/operators.csv`

Structure:

* `id` - UUID of the operator
* `name` - internal name of the operator
* `country` - country in which the operator provides carsharing services

### Locations

Locations at which the vehicles are available.

File: `/data/locations.csv`

Structure:

* `id` - UUID of the location
* `neighborhood` - neighborhood in which the location is situated
* `city` - city of the location
* `country` - country of the location

### Vehicles

Lists all vehicles of the company.

File: `/data/vehicles.csv`

Structure:

* `id` - UUID of the vehicle
* `license_plate` - registration number of the vehicle
* `model` - model of the vehicle
* `brand` - brand of the vehicle

### Users

Registry of UbeeDataSharing cusomers.

File: `/data/users.csv`

Structure:

* `id` - UUID of the user
* `first_name` - first name of the user (base64 encoded)
* `last_name` - last name of the user (base64 encoded)
* `age` - age of the user if provided upon registration (optional)
* `gender` - users' gender if they chose to provide the data upon registration

### Bookings

List of the bookings on UbeeDataSharing vehicles since January 2018

File: `/data/bookings.csv`

Structure:

* `booking_id` - UUID of the booking
* `operator_id` - UUID of the operator of the booking
* `user_id` - UUID of the user of the booking
* `vehicle_id` - UUID of the vehicle
* `location_id` - UUID of the location
* `creation_time` - time the booking was created by the user
* `start` - start of the booking
* `end` - end of the booking as requested by the user
* `real_end` - time the vehicle has been actually returned to its location
* `mileage` - km driven during the booking
* `status` - status of the booking ('finished', 'cancelled')
* `user_type` - profile of the user ('personal', 'business', 'internal' - UbeeData agents)
* `booking_type` - type of the booking ('carsharing', 'long_distance', 'service')

---

## Explore the data

Feel free to fiddle with the data and see if you can find interesting patterns of user behavior or if you have an idea about what we can change to better serve our customers. Even the sky is not a limit to your fantasy! Descriptive visualisation, statistical evaluation, machine learning; Python, R, Scala, Ruby, Matlab, Java(Script), C++, Fortran, Pascal, Basic, Assembler - whatever you like...

Clone the repo, branch, create your own folder in the `/insights` directory [use your (nick)name for clarity] and drop your code and the summary of your work and your conclusions as JupyterNB, reST, LaTeX, Word, PDF, plain text, you get the idea...

Finally, create a pull request so that we know that there's some new awesomness awaiting us!...

---

# Bridge to business

In case you were wondering what questions keep our operation and business masterminds awake at night, here are a few that the data surely must have answers to:

* how well do we know our customers, what is the profile of a "perfect" user?
* do we optimally allocate the vehicles to their parkings?
* are there activity hot spots in the cities?
* knowing the coutry-specific use patterns, what would be the outline of a perfect tariff structure?

---

## A word of warning

As any other system, UbeeDataSharing has its ups and downs and thus our data is not perfect. The system constantly evolves and sometimes bugs in the code escape our rigorous tests and lead to glitches in the data. Sometimes we just mess with the database, inadvertently introducing inconsistencies. And well, sometimed the operations teams just misconfigure the system. So be cautious in your exploration!

---

## Questions

In case you have any questions feel about us feel free to comment in the repository or just [write us directly](mailto:oleg.brovko@ubeeqo.com).